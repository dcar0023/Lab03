// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Lab03 : ModuleRules
{
	public Lab03(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
