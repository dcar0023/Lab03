// Fill out your copyright notice in the Description page of Project Settings.


#include "ChasePlayerActor.h"

// Sets default values
AChasePlayerActor::AChasePlayerActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	VisibleComponent->SetupAttachment(RootComponent);
	ConstructorHelpers::FObjectFinder<UStaticMesh>
		CubeMeshObj(TEXT("/Game/Geometry/Meshes/1M_Cube.1M_Cube"));
	if (CubeMeshObj.Succeeded()) {
		VisibleComponent->SetStaticMesh(CubeMeshObj.Object);
	}
	Speed = 200.0f;
	MinimumDistance = 150.0f;
}

// Called when the game starts or when spawned
void AChasePlayerActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AChasePlayerActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (FollowTarget != NULL) {
		FVector TargetLocation = FollowTarget->GetActorLocation();
		FVector CurrentPosition = GetActorLocation();

		FVector TargetDirection = TargetLocation - CurrentPosition;

		if (FVector::Dist(CurrentPosition, TargetLocation) > MinimumDistance) {
			TargetDirection.Normalize();
			CurrentPosition += TargetDirection * Speed * DeltaTime;
		}
		SetActorLocation(CurrentPosition);
	}
}

