// Fill out your copyright notice in the Description page of Project Settings.


#include "SurveillanceActor.h"

// Sets default values
ASurveillanceActor::ASurveillanceActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	VisibleComponent->SetupAttachment(RootComponent);
	ConstructorHelpers::FObjectFinder<UStaticMesh>
		CubeMeshObj(TEXT("/Game/Geometry/Meshes/1M_Cube.1M_Cube"));
	if (CubeMeshObj.Succeeded()) {
		VisibleComponent->SetStaticMesh(CubeMeshObj.Object);
	}

	ConstructorHelpers::FObjectFinder<UMaterial>
		OffMaterialObj(TEXT("/Game/StarterContent/Materials/M_Tech_Hex_Tile.M_Tech_Hex_Tile"));
	if (OffMaterialObj.Succeeded()) {
		OffMaterial = OffMaterialObj.Object;
	}

	ConstructorHelpers::FObjectFinder<UMaterial>
		OnMaterialObj(TEXT("/Game/StarterContent/Materials/M_Tech_Hex_Tile_Pulse.M_Tech_Hex_Tile_Pulse"));
	if (OnMaterialObj.Succeeded()) {
		OnMaterial = OnMaterialObj.Object;
	}

	MaxDistance = 500.0f;
}

// Called when the game starts or when spawned
void ASurveillanceActor::BeginPlay()
{
	Super::BeginPlay();
	VisibleComponent->SetMaterial(0, OffMaterial);
}

// Called every frame
void ASurveillanceActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (TargetActor != NULL) {
		FVector TargetLocation = TargetActor->GetActorLocation();
		FVector CurrentPosition = GetActorLocation();

		FVector TargetDirection = TargetLocation - CurrentPosition;

		if (FVector::Dist(CurrentPosition, TargetLocation) < MaxDistance) {
			TargetDirection.Normalize();

			float DotProduct = FVector::DotProduct(GetActorForwardVector(), TargetDirection);
			if (DotProduct > 0.9f) {
				VisibleComponent->SetMaterial(0, OnMaterial);
			}
			else {
				VisibleComponent->SetMaterial(0, OffMaterial);
			}
		} else {
		    // turns off when player is out of range
            VisibleComponent->SetMaterial(0, OffMaterial);
		}
		SetActorLocation(CurrentPosition);
	}
}

