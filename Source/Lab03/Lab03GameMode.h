// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Lab03GameMode.generated.h"

UCLASS(minimalapi)
class ALab03GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALab03GameMode();
};



