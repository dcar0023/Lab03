// Copyright Epic Games, Inc. All Rights Reserved.

#include "Lab03GameMode.h"
#include "Lab03Character.h"
#include "UObject/ConstructorHelpers.h"

ALab03GameMode::ALab03GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
